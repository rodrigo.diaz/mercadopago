# Proyecto: bymovi/mercadopago

## Instalación

Correr el siguiente comando

```sh
composer require bymovi/mercadopago
```
Buscar la clave `providers` en `app/config/app.php` y registrar el Service Provider

```php
    'providers' => array(
        // ...
        'Bymovi\Mercadopago\MercadopagoServiceProvider',
    )
```

Encuentra la clave `aliases` en `app/config/app.php` y agrega el siguiente código.

```php
    'aliases' => array(
        // ...
        'Mercadopago' => 'Bymovi\Mercadopago\Facades\Mercadopago',
    )
```